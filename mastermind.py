import requests as rq 
import random
import json
from collections import Counter
from itertools import combinations as nPr

mm_url = "https://we6.talentsprint.com/wordle/game/"
session = rq.Session()

def regis_bot():
    register_url = mm_url + "register"
    register_dict = {"mode": "mastermind", "name": "Our_Bot-15,89,126"}
    return register_url, register_dict

def store_cookie(register_url, register_dict):
    r = rq.post(register_url, json=register_dict)
    resp = session.post(register_url, json=register_dict)
    message = resp.json()['id']
    return message

def create_using_cookie(message):
    creat_url = mm_url + "create"
    creat_dict = {"id": message, "overwrite": True}
    rc = session.post(creat_url, json=creat_dict)
    return rc.json()

#print(load_data("5letters.txt"))

def send_guess(message, guess):
    guess_url = mm_url + "guess"
    guess_dict = {"id": message, "guess": guess}
    response = rq.post(guess_url, json=guess_dict)
    return response.json()


def load_data(word_file):
    return [word.strip() for word in open(word_file)]

<<<<<<< HEAD
<<<<<<< HEAD
#Read the target word from the file
    target_word = load_data(word_file)
    print(f"Target word: {target_word}")

    # Read the words list from another file
    with open(words_list_file, 'r') as file:
        words_list = file.read().splitline()

# Function to select a random word
def select_random_word(words):
    return random.choice(words)

#Count the matches
    match_count = count_matches(target_word, words_list)
    print(f"The word '{target_word}' appears {match_count} times in the list.")

# Function to filter words containing any letters from the given word
def segregate_words(words, selected_word):
    segregated_words = [word for word in words if all(letter not in word for letter in selected_word)]
    return segregated_words

# Read the target word from the file
word_file = '5letters.txt'  
target_word = load_data(word_file)
print(f"Target word: {target_word}")

# Read the words list from another file
words_list_file = '5letters.txt' 
with open(words_list_file, 'r') as file:
    words_list = file.read().splitlines()
>>>>>>> 88f9e3b2e073e7833513a2fac7e1d1e2f0f75bd2
=======
def guessing_algo(word_file):
    possible_words = load_data(word_file)
    register_url, register_dict = regis_bot()
    create_response = create_using_cookie(store_cookie(register_url, register_dict))

    for word in possible_words:
            guess_response = send_guess(create_response, word)
            print(f"Guess '{word}' Response:", guess_response)
            
            if guess_response.get('message') == 'win':
                break

print(guessing_algo("5letters.txt"))
>>>>>>> 378c72d4a2ac5ca7e34e41d50d3d08c401dd7720
