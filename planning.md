# Planning

Brute force:

---
Run through the entire 5 letter words until all 5 letters match
<br>
<br>
<br>

Slightly better:

---

1) Start with a word which has all 5 letters different
<br>

2) Using the feedback given: When feedback == 0; remove those specific letters and check words which do not have those letters



snehachoudhary@Snehas-MacBook-Air ~ % python3
Python 3.12.4 (main, Jun  6 2024, 18:26:44) [Clang 15.0.0 (clang-1500.1.0.2.5)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import requests as rq
>>> import json
>>> mm_url = "https://we6.talentsprint.com/wordle/game/"
>>> register_url = mm_url + "register/"
>>> register_dict = {"mode": "Mastermind", "name":"Sneha"}
>>> register_with = json.dumps(register_dict)
>>> register_with
'{"mode": "Mastermind", "name": "Sneha"}'
>>> rq.post(register_url, json = register_dict)
<Response [404]>
>>> register_url = mm_url + "register"
>>> rq.post(register_url, json = register_dict)
<Response [201]>
>>> rq
<module 'requests' from '/opt/homebrew/lib/python3.12/site-packages/requests/__init__.py'>
>>> r = rq.post(register_url, json=register_dict)
>>> r.text
'{"id":"b809f924-3077-11ef-9113-f23c94110b38","message":"Created"}\n'
>>> r.json
<bound method Response.json of <Response [201]>>
>>> r.json()
{'id': 'b809f924-3077-11ef-9113-f23c94110b38', 'message': 'Created'}
>>> r.json()['id']
'b809f924-3077-11ef-9113-f23c94110b38'
>>> me = r.json()['id']
>>> create_url = mm_url + "create"
>>> create_dict = {"id" : me, "overwrite": True}
>>> rc = rq.post(cre
... create_dict create_url credits()
  File "<stdin>", line 1
    rc = rq.post(cre
                 ^^
SyntaxError: invalid syntax. Perhaps you forgot a comma?
>>> rc = rq.post(create_url, json=create_dict)
>>> rc.json()
{'error': '400 Bad Request: Session cookie missing or broken. Make sure to pass on the cookie returned by /register'}
>>> session = rq.Session()
>>> session.post(register_url, json=register_dict)
<Response [201]>
>>> resp = session.post(register_url, json=register_dict)
>>> resp.json()
{'id': '66eff4f2-3078-11ef-99ca-f23c94110b38', 'message': 'Created'}
>>> me = resp.json()['id']
>>> me
'66eff4f2-3078-11ef-99ca-f23c94110b38'
>>> rc = session.post(create_url, json=create_dict)
>>> rc.json()
{'created': False, 'message': 'Register yourself with /register first'}
>>> resp = session.post(register_url, json=register_dict)
>>> me = resp.json()['id']
>>> rc = session.post(create_url, json=create_dict)
>>> 
>>> rc.json()
{'created': False, 'message': 'Register yourself with /register first'}
>>> 
